package ru.eka.files;

import java.io.*;

/**
 * Класс для нахождения счастливых чисел и записи их в файл "txt6data.dat"
 *
 * @author Куцкая Э.А., 15ОИТ18
 */
public class HappyNumber {
    public static void main(String[] args) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("src\\ru\\eka\\files\\int6data.dat"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\eka\\files\\txt6data.dat"));
        int numbers;

        while (dataInputStream.available() > 0) {
            numbers = dataInputStream.readInt();
            if (isHappy(numbers)) {
                bufferedWriter.write(String.valueOf(numbers) + " ");
            }
        }
        dataInputStream.close();
        bufferedWriter.close();
    }

    /**
     * Метод для нахождения счастливого числа
     *
     * @param numbers
     * @return true
     */
    private static boolean isHappy(int numbers) {
        return (numbers / 100000 + numbers % 100000 / 10000 + numbers % 10000 /1000) == (numbers % 1000 / 100 + numbers % 100 / 10 + numbers % 10);
    }
}
