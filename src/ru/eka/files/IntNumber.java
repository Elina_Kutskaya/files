package ru.eka.files;

import java.io.*;


/**
 * Класс для заполнения файла "intdata.dat" данными из текстового файла "input.txt"
 *
 * @author Куцкая Э.А., 15ОИТ18
 */
public class IntNumber {
    public static void main(String[] args) throws IOException {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\eka\\files\\input.txt"));
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("src\\ru\\eka\\files\\intdata.dat"));
            String number;

            while ((number = bufferedReader.readLine()) != null) {
                dataOutputStream.writeInt(Integer.valueOf(number));
            }
            bufferedReader.close();
            dataOutputStream.close();
        }catch (IOException | NumberFormatException e){
            System.out.println(e.getMessage());
        }
    }
}
