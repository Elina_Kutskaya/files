package ru.eka.files;

import java.io.*;

/**
 *  Класс для заполнения файла "int6data.dat" шестизначными числами
 *
 * @author Куцкая Э.А., 15ОИТ18
 */
public class SixNumbers {
    public static void main(String[] args) throws IOException {
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream("src\\ru\\eka\\files\\intdata.dat"));
            DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("src\\ru\\eka\\files\\int6data.dat"));
            int numbers;

            while (dataInputStream.available() != 0) {
                numbers = dataInputStream.readInt();
                if(numbers == 0){
                    dataOutputStream.writeInt(numbers);
                }else if (numbers > 999 && numbers < 1000000){
                    dataOutputStream.writeInt(numbers);
                }
            }
            dataInputStream.close();
            dataOutputStream.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
